package com.example.login

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.login.ui.theme.LoginTheme
import kotlin.random.Random

class Botones : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LoginTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Change()
                }
            }
        }
    }
}

@Composable
fun Change() {
    var num by remember {
        mutableStateOf(0)
    }
    var nvl by remember {
        mutableStateOf(0)
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Spacer(modifier = Modifier.height(16.dp))
        val primaryColor = Color.Red
        val secondaryColor = Color.White
        if (num == 1) {
            nvl = Random.nextInt(1, 17)
            Surface(
                color = primaryColor,
                shape = RoundedCornerShape(8.dp),
                contentColor = secondaryColor,
                modifier = Modifier.padding(top = 40.dp)

            ) {
                Text(
                    "Turtwig level $nvl",
                    color = secondaryColor,
                    modifier = Modifier.padding(16.dp)
                )
            }
        } else if (num == 2) {
            nvl = Random.nextInt(18, 31)
            Surface(
                color = primaryColor,
                shape = RoundedCornerShape(8.dp),
                contentColor = secondaryColor,
                modifier = Modifier.padding(top = 40.dp)

            ){
                Text("Grotle level $nvl",
                    color = secondaryColor,
                    modifier = Modifier.padding(16.dp)
                )
            }

        } else if (num == 3) {
            nvl = Random.nextInt(32, 100)
            Surface(
                color = primaryColor,
                shape = RoundedCornerShape(8.dp),
                contentColor = secondaryColor,
                modifier = Modifier.padding(top = 40.dp)

            ) {
                Text(
                    "Torterra level $nvl",
                    color = secondaryColor,
                    modifier = Modifier.padding(16.dp)
                )
            }
        }

        Spacer(modifier = Modifier.weight(1f))
        Fotos(num = num)
        Spacer(modifier = Modifier.weight(1f))
        ImageButton(modifier = Modifier
            .padding(16.dp)
            .padding(bottom = 100.dp),
            onClick = {
                num++
                if (num > 3) {
                    num = 0
                }
            }
        )

    }
}

@Composable
fun ImageButton(modifier: Modifier = Modifier, onClick: () -> Unit) {

    var imgButton by remember {
        mutableStateOf(R.drawable.pokeballbutton)
    }

    Image(
        painter = painterResource(id = imgButton),
        contentDescription = "Botón imagen",
        modifier = modifier
            .size(100.dp)
            .clickable { onClick.invoke() }
    )
}

@Composable
fun Fotos(num: Int) {
    var img by remember {
        mutableStateOf(R.drawable.pokeball)
    }

    LaunchedEffect(num) {
        when (num) {
            0 -> img = R.drawable.pokeball
            1 -> img = R.drawable.turtwig
            2 -> img = R.drawable.grotle
            3 -> img = R.drawable.torterra
        }
    }

    Image(painter = painterResource(id = img), contentDescription = "Pokémon")
}

@Preview(showBackground = true)
@Composable
fun PreviewMain() {
    Change()
}